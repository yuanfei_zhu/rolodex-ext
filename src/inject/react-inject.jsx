var ContactTab = React.createClass({
	getInitialState: function() {
		return {
			contactCnt: '42',
			shouldContactListViewOpen: true,
			topContacts: [],
			shownContactCnt: 5
		}
	},
	randomlyMarkContactAsSavedInSalesforce: function(contacts) {
		return _.map(contacts, function(contact) {
			var r = _.random();
			if (r < 0.8) {
				contact.savedInSalesforce = true;
			}

			return contact;
		});
	},
	componentDidMount: function() {
		var companyQueryUrl = '/sales/search/results/people?start=0&count=10&pivotType=EMPLOYEE&pivotId=';
		var url = companyQueryUrl + this.props.companyId;
		$.get(url, function(data) {
			var contactCnt = data.pagination.total;
			var contacts = data.result.searchResults;

			contacts = this.randomlyMarkContactAsSavedInSalesforce(contacts);

			if (this.isMounted()) {
				this.setState({
					contactCnt: contactCnt,
					contacts: contacts
				})
			}
		}.bind(this));
	},
	toggleContactListView: function() {
		this.setState(function(ps) {
			return {
				shouldContactListViewOpen: !ps.shouldContactListViewOpen
			};
		})
	},
	loadMoreContacts: function() {
		console.log('loadMoreContacts');
		this.setState(function(ps) {
			return {
				shownContactCnt: ps.shownContactCnt + 5
			}
		})
	},
	toggleSyncedContacts: function() {
		this.setState(function(ps) {
			return {
				shouldShowSyncedContacts: !ps.shouldShowSyncedContacts
			};
		});
	},
	render: function() {
		var topContacts = _.take(_.filter(this.state.contacts, function(contact) {
			if (!this.state.shouldShowSyncedContacts) {
				return !contact.savedInSalesforce;
			}
			return true;
		}.bind(this)), this.state.shownContactCnt);
		var contactTabStyle = {
			marginLeft: 90,
			overflow: 'visible'
		};

		var toggleSyncedContactsMenuText = (this.state.shouldShowSyncedContacts) ? 'Hide Synced Contacts' : 'Show Synced Contacts';

		return (<dl className="snippet">
					<dt className="label">Contacts{this.shownContactCnt}</dt>
					<dd style={contactTabStyle}>
						<p>
							<a onClick={this.toggleContactListView}>found <b>{this.state.contactCnt}</b> contacts</a>,
							companyID = <b>{this.props.companyId}</b>,
							topContacts = <b>{this.state.topContacts.length}</b>,
							<a onClick={this.toggleSyncedContacts}>{toggleSyncedContactsMenuText}</a>
						</p>
						<p>
							{(() => {
								if (this.state.shouldContactListViewOpen) {
									return topContacts.map(function(contact) {
										return <ContactThumbnail contact={contact} />;
									})
								}
							})()}
						</p>
						<p className="es-load-more-handle"><a onClick={this.loadMoreContacts}>More</a></p>
					</dd>
				</dl>);
	}
});

var ContactThumbnail = React.createClass({
	contactDescription: function(contact) {
		return JSON.stringify(contact);
	},
	imageUrl: function() {
		var size = 50;
		var url = this.props.contact.imageUri;

		url = url && url.replace('shrink_xx_yy', 'shrink_' + size + '_' + size);

		return url;
	},
	mediaStyle: {
		flex: '2 100%'
	},
	actionStyle: {
		width: 15,
		alignSelf: 'center'
	},
	handleChange: function(event) {
		PubSub.publish('contactSelectedStatusChanged', {
			status: event.target.checked,
			contact: this.props.contact
		});
		console.log(event.target.checked, this.props.contact);
	},
	render: function() {
		return (<div className="es-contact-thumbnail">
					<a target="_BLANK" href={this.props.contact.profileUrl} className="es-image-wrapper"><img src={this.imageUrl()} /></a>
					<div className="es-contact-media" style={this.mediaStyle}>
						<div>
							<b>{this.props.contact.formattedName}</b>
							<SalesforceStatus contact={this.props.contact} />
							<a className="es-feedback-action">(like)</a>
							<a className="es-feedback-action">(dislike)</a>
						</div>
						<p>{this.props.contact.headline}</p>
					</div>
					<div className="es-contact-action" style={this.actionStyle}>
						<input type="checkbox" onChange={this.handleChange} />
					</div>
				</div>)
	}
});

var SalesforceStatus = React.createClass({
	getInitialState: function() {
		return {
			user: '',
			updated_at: '',

		}
	},
	showSfStatusPopup: function() {
		this.setState({
			user: 'EverString SDR #1',
			updated_at: Date(),

			shouldShowPopup: true
		})
	},
	hideSfStatusPopup: function() {
		this.setState({
			shouldShowPopup: false
		});
	},
	render: function() {
		var popupStyle = {
			display: this.state.shouldShowPopup ? 'block' : 'none',
			position: 'absolute',
			top: 0,
			left: 10,
		    backgroundColor: 'white',
		    width: 200,
		    padding: 10,
		   	border: '1px solid gainsboro',
		    boxShadow: 'rgba(0, 0, 0, 0.1) 1px 1px 1px',
		    zIndex: 10
		};

		var containerStyle = {
			position: 'relative',
			display: 'inline-block',
			marginLeft: 10
		};

		var contact = this.props.contact;
		if (contact) {
			if (contact.savedInSalesforce) {
				return (<div onMouseEnter={this.showSfStatusPopup}
							 onMouseLeave={this.hideSfStatusPopup}
							 style={containerStyle}>
							<i className="fa fa-cloud"></i>
							<div className="es-sf-status-popup" style={popupStyle}>
								<p><b>Saved in Salesforce</b> <a>(view record)</a></p>
								<dl>
									<dt>Who</dt> <dd>{this.state.user}</dd>
									<dt>When</dt> <dd>{this.state.updated_at}</dd>
								</dl>
							</div>
						</div>);
			}
		}

		return null;
	}
});

var ContactCart = React.createClass({
	getInitialState: function() {
		return {
			contacts: [],
			shouldSavedContactListOpen: false,
		};
	},
	componentDidMount: function() {
		var that = this;
		PubSub.subscribe('contactSelectedStatusChanged', function(msg, data) {
			that.setState(function(ps) {
				var newContacts = ps.contacts;
				// selected contact
				if (data.status) {
					newContacts.push(data.contact);
				}
				// unselected contact
				else {
					// find the contact, remove it
					newContacts = _.filter(newContacts, function(contact) {
						return contact !== data.contact;
					});
				}

				return {
					contacts: newContacts
				}
			})
		});
	},
	componentWillUnmount: function() {
		PubSub.unsubscribe('contactSelectedStatusChanged');
	},
	toggleSavedContactList: function() {
		this.setState(function(ps) {
			return {
				shouldSavedContactListOpen: !ps.shouldSavedContactListOpen
			};
		});
	},
	render: function() {
		return (
			<div className="nav-item">
				<a className="nav-link" onClick={this.toggleSavedContactList}>Saved Contacts: {this.state.contacts.length}</a>
				{(() => {
					if (this.state.shouldSavedContactListOpen) {
						return (<div className="es-saved-contact-list-container">
									<div className="es-action-panel">
										<button type="button" className="es-action">Export CSV</button>
										<button style={{marginLeft: 27}} type="button" className="es-action">Sync to Salesforce</button>
									</div>
									<ul className="es-saved-contact-list">
									{(() => {
										return this.state.contacts.map((contact) => {
											/* return (<li>{contact.formattedName}</li>); */
											return (<li><ContactThumbnail contact={contact} /></li>);
										});
									})()}
							    	</ul>
							    </div>)
					}
				 })()}
			</div>
		);
	}
});


$(function() {
	function getCompanyIdByNode(node) {
		var companyInsightUrl = node.querySelector('.name a').href;
		var m = /companyId=(\d+)/g.exec(companyInsightUrl);

		return m && m[1];
	}

	var nodeList = document.querySelectorAll('.results .entity .body');

	// enrich contact info for companies
	_.each(nodeList, function(node) {
		var contactNode = document.createElement('DIV');
		contactNode.className = 'es-contacts';

		var companyId = getCompanyIdByNode(node);

		React.render(<ContactTab companyId={companyId} />, contactNode);
		node.appendChild(contactNode);
	});

	// add contact cart
	var cartNode = document.createElement('DIV');
	cartNode.className = 'es-contact-cart';
	React.render(<ContactCart />, cartNode);
	document.getElementById('navigations').appendChild(cartNode);
});