var ContactTab = React.createClass({displayName: "ContactTab",
	getInitialState: function() {
		return {
			contactCnt: '42',
			shouldContactListViewOpen: true,
			topContacts: [],
			shownContactCnt: 5
		}
	},
	randomlyMarkContactAsSavedInSalesforce: function(contacts) {
		return _.map(contacts, function(contact) {
			var r = _.random();
			if (r < 0.8) {
				contact.savedInSalesforce = true;
			}

			return contact;
		});
	},
	componentDidMount: function() {
		var companyQueryUrl = '/sales/search/results/people?start=0&count=10&pivotType=EMPLOYEE&pivotId=';
		var url = companyQueryUrl + this.props.companyId;
		$.get(url, function(data) {
			var contactCnt = data.pagination.total;
			var contacts = data.result.searchResults;

			contacts = this.randomlyMarkContactAsSavedInSalesforce(contacts);

			if (this.isMounted()) {
				this.setState({
					contactCnt: contactCnt,
					contacts: contacts
				})
			}
		}.bind(this));
	},
	toggleContactListView: function() {
		this.setState(function(ps) {
			return {
				shouldContactListViewOpen: !ps.shouldContactListViewOpen
			};
		})
	},
	loadMoreContacts: function() {
		console.log('loadMoreContacts');
		this.setState(function(ps) {
			return {
				shownContactCnt: ps.shownContactCnt + 5
			}
		})
	},
	toggleSyncedContacts: function() {
		this.setState(function(ps) {
			return {
				shouldShowSyncedContacts: !ps.shouldShowSyncedContacts
			};
		});
	},
	render: function() {
		var topContacts = _.take(_.filter(this.state.contacts, function(contact) {
			if (!this.state.shouldShowSyncedContacts) {
				return !contact.savedInSalesforce;
			}
			return true;
		}.bind(this)), this.state.shownContactCnt);
		var contactTabStyle = {
			marginLeft: 90,
			overflow: 'visible'
		};

		var toggleSyncedContactsMenuText = (this.state.shouldShowSyncedContacts) ? 'Hide Synced Contacts' : 'Show Synced Contacts';

		return (React.createElement("dl", {className: "snippet"}, 
					React.createElement("dt", {className: "label"}, "Contacts", this.shownContactCnt), 
					React.createElement("dd", {style: contactTabStyle}, 
						React.createElement("p", null, 
							React.createElement("a", {onClick: this.toggleContactListView}, "found ", React.createElement("b", null, this.state.contactCnt), " contacts"), "," + ' ' +
							"companyID = ", React.createElement("b", null, this.props.companyId), "," + ' ' +
							"topContacts = ", React.createElement("b", null, this.state.topContacts.length), ",", 
							React.createElement("a", {onClick: this.toggleSyncedContacts}, toggleSyncedContactsMenuText)
						), 
						React.createElement("p", null, 
							(() => {
								if (this.state.shouldContactListViewOpen) {
									return topContacts.map(function(contact) {
										return React.createElement(ContactThumbnail, {contact: contact});
									})
								}
							})()
						), 
						React.createElement("p", {className: "es-load-more-handle"}, React.createElement("a", {onClick: this.loadMoreContacts}, "More"))
					)
				));
	}
});

var ContactThumbnail = React.createClass({displayName: "ContactThumbnail",
	contactDescription: function(contact) {
		return JSON.stringify(contact);
	},
	imageUrl: function() {
		var size = 50;
		var url = this.props.contact.imageUri;

		url = url && url.replace('shrink_xx_yy', 'shrink_' + size + '_' + size);

		return url;
	},
	mediaStyle: {
		flex: '2 100%'
	},
	actionStyle: {
		width: 15,
		alignSelf: 'center'
	},
	handleChange: function(event) {
		PubSub.publish('contactSelectedStatusChanged', {
			status: event.target.checked,
			contact: this.props.contact
		});
		console.log(event.target.checked, this.props.contact);
	},
	render: function() {
		return (React.createElement("div", {className: "es-contact-thumbnail"}, 
					React.createElement("a", {target: "_BLANK", href: this.props.contact.profileUrl, className: "es-image-wrapper"}, React.createElement("img", {src: this.imageUrl()})), 
					React.createElement("div", {className: "es-contact-media", style: this.mediaStyle}, 
						React.createElement("div", null, 
							React.createElement("b", null, this.props.contact.formattedName), 
							React.createElement(SalesforceStatus, {contact: this.props.contact}), 
							React.createElement("a", {className: "es-feedback-action"}, "(like)"), 
							React.createElement("a", {className: "es-feedback-action"}, "(dislike)")
						), 
						React.createElement("p", null, this.props.contact.headline)
					), 
					React.createElement("div", {className: "es-contact-action", style: this.actionStyle}, 
						React.createElement("input", {type: "checkbox", onChange: this.handleChange})
					)
				))
	}
});

var SalesforceStatus = React.createClass({displayName: "SalesforceStatus",
	getInitialState: function() {
		return {
			user: '',
			updated_at: '',

		}
	},
	showSfStatusPopup: function() {
		this.setState({
			user: 'EverString SDR #1',
			updated_at: Date(),

			shouldShowPopup: true
		})
	},
	hideSfStatusPopup: function() {
		this.setState({
			shouldShowPopup: false
		});
	},
	render: function() {
		var popupStyle = {
			display: this.state.shouldShowPopup ? 'block' : 'none',
			position: 'absolute',
			top: 0,
			left: 10,
		    backgroundColor: 'white',
		    width: 200,
		    padding: 10,
		   	border: '1px solid gainsboro',
		    boxShadow: 'rgba(0, 0, 0, 0.1) 1px 1px 1px',
		    zIndex: 10
		};

		var containerStyle = {
			position: 'relative',
			display: 'inline-block',
			marginLeft: 10
		};

		var contact = this.props.contact;
		if (contact) {
			if (contact.savedInSalesforce) {
				return (React.createElement("div", {onMouseEnter: this.showSfStatusPopup, 
							 onMouseLeave: this.hideSfStatusPopup, 
							 style: containerStyle}, 
							React.createElement("i", {className: "fa fa-cloud"}), 
							React.createElement("div", {className: "es-sf-status-popup", style: popupStyle}, 
								React.createElement("p", null, React.createElement("b", null, "Saved in Salesforce"), " ", React.createElement("a", null, "(view record)")), 
								React.createElement("dl", null, 
									React.createElement("dt", null, "Who"), " ", React.createElement("dd", null, this.state.user), 
									React.createElement("dt", null, "When"), " ", React.createElement("dd", null, this.state.updated_at)
								)
							)
						));
			}
		}

		return null;
	}
});

var ContactCart = React.createClass({displayName: "ContactCart",
	getInitialState: function() {
		return {
			contacts: [],
			shouldSavedContactListOpen: false,
		};
	},
	componentDidMount: function() {
		var that = this;
		PubSub.subscribe('contactSelectedStatusChanged', function(msg, data) {
			that.setState(function(ps) {
				var newContacts = ps.contacts;
				// selected contact
				if (data.status) {
					newContacts.push(data.contact);
				}
				// unselected contact
				else {
					// find the contact, remove it
					newContacts = _.filter(newContacts, function(contact) {
						return contact !== data.contact;
					});
				}

				return {
					contacts: newContacts
				}
			})
		});
	},
	componentWillUnmount: function() {
		PubSub.unsubscribe('contactSelectedStatusChanged');
	},
	toggleSavedContactList: function() {
		this.setState(function(ps) {
			return {
				shouldSavedContactListOpen: !ps.shouldSavedContactListOpen
			};
		});
	},
	render: function() {
		return (
			React.createElement("div", {className: "nav-item"}, 
				React.createElement("a", {className: "nav-link", onClick: this.toggleSavedContactList}, "Saved Contacts: ", this.state.contacts.length), 
				(() => {
					if (this.state.shouldSavedContactListOpen) {
						return (React.createElement("div", {className: "es-saved-contact-list-container"}, 
									React.createElement("div", {className: "es-action-panel"}, 
										React.createElement("button", {type: "button", className: "es-action"}, "Export CSV"), 
										React.createElement("button", {style: {marginLeft: 27}, type: "button", className: "es-action"}, "Sync to Salesforce")
									), 
									React.createElement("ul", {className: "es-saved-contact-list"}, 
									(() => {
										return this.state.contacts.map((contact) => {
											/* return (<li>{contact.formattedName}</li>); */
											return (React.createElement("li", null, React.createElement(ContactThumbnail, {contact: contact})));
										});
									})()
							    	)
							    ))
					}
				 })()
			)
		);
	}
});


$(function() {
	function getCompanyIdByNode(node) {
		var companyInsightUrl = node.querySelector('.name a').href;
		var m = /companyId=(\d+)/g.exec(companyInsightUrl);

		return m && m[1];
	}

	var nodeList = document.querySelectorAll('.results .entity .body');

	// enrich contact info for companies
	_.each(nodeList, function(node) {
		var contactNode = document.createElement('DIV');
		contactNode.className = 'es-contacts';

		var companyId = getCompanyIdByNode(node);

		React.render(React.createElement(ContactTab, {companyId: companyId}), contactNode);
		node.appendChild(contactNode);
	});

	// add contact cart
	var cartNode = document.createElement('DIV');
	cartNode.className = 'es-contact-cart';
	React.render(React.createElement(ContactCart, null), cartNode);
	document.getElementById('navigations').appendChild(cartNode);
});