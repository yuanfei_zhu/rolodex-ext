// add count=100 to search requests
chrome.webRequest.onBeforeRequest.addListener(function (req) {
	var url = new URL(req.url);
	if (url.pathname.startsWith('/sales/search')) {
		var num = 100;
		var cntParam = 'count=' + num;
		// check if there is already a `count` parameter
		if (url.search.search('count=') >= 0) {
			if (url.search.search(cntParam) === -1) {
				url.search = url.search.replace(/count=\d+/, cntParam);
			}
		}
		// original request doesn't contain a count parameter
		else {
			url.search += ('&' + cntParam);
		}

		// modify the request with count parameter
		return { redirectUrl: url.href };
	}
}, {
    urls: ["https://www.linkedin.com/*"]
}, ["blocking"]);


chrome.runtime.onInstalled.addListener(function() {
  var context = "selection";
  var title = "Rolodex: Like this";
  var id = chrome.contextMenus.create({"title": title, "contexts":[context],
                                         "id": "context" + context});
});

// add click event
chrome.contextMenus.onClicked.addListener(onClickHandler);

// The onClicked callback function.
function onClickHandler(info, tab) {
  var sText = info.selectionText;
  var url = "https://www.google.com/search?q=" + encodeURIComponent(sText);
  window.open(url, '_blank');
}