module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            reload: {
              files: ['src/**/*.js'],
              tasks: ['open']
            },
            options: {
                'spawn': true,
                'interrupt': false,
                'debounceDelay': 500,
                'interval': 100,
                'event': 'all',
                'reload': false,
                'forever': true,
                'dateFormat': null,
                'atBegin': false,
                'livereload': false,
                'cwd': process.cwd(),
                'livereloadOnError': true
            }
        },
        open: {
            reload: {
                path: 'http://reload.extensions',
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-open');

    grunt.registerTask('default', ['watch']);
};